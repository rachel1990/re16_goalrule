\section{Feature Division and Relationship Modeling}
\label{sec:featureDivision}
To facilitate the integrated planning process, the learner divides the whole configurable system feature set $F$ into two sets: a goal-related feature set $F_{goal}$ to be handled by \emph{goal-oriented planning}, and a rule-based feature set $F_{rule}$ to be handled by \emph{rule-based planning}. 
Apart from this, the learner is also responsible for modeling the relation between goals and the goal-related system features, which is necessary for the goal-oriented planning.  

These two processes are executed on different feature sets in different frequencies:
\begin{itemize}
\item The feature division process is performed on the configurable system features with respect to the goals. This process shall be executed when the system is initially deployed,
% to enable the further integrated planning process, 
and again when changes happen to the feature set or the goal set. We assume that the significant features for a specific goal remain unchanged when the system features and goals hold steady.  

\item The relationship modeling process uses functions to model the relationship between goals and the goal-related features. This process shall be executed when the leant functions are not accurate enough to predict a goal satisfaction degree based on the current feature values.     
\end{itemize}

\subsection{Feature Division}
\label{subsec:division}
%The whole feature set $F$ is divided into the set $F_{goal}$ and the set $F_{rule}$.
%Features in $F_{goal}$ will be handled by the goal-oriented planning, while features in $F_{rule}$ will be handled by the rule-based planning. 
Features are divided into two sets based on their impacts on goals:
a feature $f$ ($f$ $\in$ $F$) is included in $F_{goal}$ only if it has significant impacts on one or more goals in the goal set $G$, otherwise it will be included in $F_{rule}$ and become irrelevant to the goal-oriented planning process.
%To find out the significant features, the learner needs to: 1) collects numbers of records stating the values of features and the goals, 

In order to find out the significant features, numbers of records shall be collected to run significance tests. 
A record $r$ consists of the values of features and the satisfaction degree of user goals at that point, 
 \[
 \bb
r=\{v_{f_1}, v_{f_2}, ... ,v_{g_1},v_{g_2}...\}
 \ee
\]
where $v_{f}$ denotes the value of feature $f$ and $v_{g}$ denotes the satisfaction value of goal $g$.
$v_{f}$  is an integer that is mapped from an enum feature value according to predefined mapping relationship. For example, the three enum values of \emph{LogInLog} ($f_{0}$), full, partial and none, can be mapped to 1, 2 and 3 respectively.  
$v_{g}$ is calculated from goal related properties. For example, the satisfaction degree of goal \emph{quick browse response} is calculated from the value of browse response time.
The less the browse response time, the larger the satisfaction degree of \emph{quick browse response}.

Significance tests are then executed based on the collected records.
One significance test is performed for each goal $g$ ($g$ $\in$ $G$), where $g$ acts as dependent variable and all the features in set F act as independent variables. 
A significance test produces a $p$-$value$ for each feature, indicating the probability of the hypothesis that the feature is not significant for $g$.
If the $p$-$value$ is less than or equal to the predefined significance level $\alpha$, the corresponding feature is considered statistically significant for $g$.
Such features will be included in $F_{goal}^{g}$, the goal-related feature set for $g$, and accordingly included in $F_{goal}$. 
In statistical methods, the significance level $\alpha$ is often chosen to be equal to 0.01, 0.05 or 0.10, but any value between 0 and 1 can be used.
Here the value of $\alpha$ shall be determined by partly referring to the commonly used values in statistical methods. However, such value varies according to
different applications scenarios.
%A significance level $\alpha$ is predefined for rejecting the null hypothesis that a feature is not significant; 
If a feature is significant for at least one goal under $\alpha$,  it will be included in the goal set.

\textbf{Example.} Back to our running example, suppose 24 features as shown in Figure \ref{fig:cfm}, and user   
goals of  \emph{quick browse response ($g_1$)}, \emph{quick order generation ($g_2$)}, \emph{quick payment response ($g_3$)}, \emph{high usability ($g_4$)},  \emph{high security ($g_5$)} and \emph{high reliabilty ($g_6$)}.

The system divides the features into two sets through significance tests, with a predefined significance level $\alpha$ 0.0025.
It collected numbers of observed records as shown in Table \ref{tab:records}, where one record consists of 24 feature values together with 6 goal satisfaction degrees.
\begin{table}[htdp]
\caption{Collected Records for Significance Tests}
\begin{center}
\begin{tabular}{|c|c|c|c|c|c|c|c|c|c|}
\hline
$f_0$&$f_1$&$f_2$&$f_3$&$f_4$&...& $g_1$&$g_2$&$g_3$&...\\
\hline
3&3&1&2&2&...&84.9&49.8&7.8&...\\
\hline
3&3&1&2&2&...&84.9&49.8&7.8&...\\
\hline
3&3&1&2&2&...&84.9&49.8&7.8&...\\
\hline
3&3&1&2&2&...&84.9&49.8&7.8&...\\
\hline
\end{tabular}
\end{center}
\label{tab:records}
\end{table}%

% \[
 %\bb
%r_1=\{3, 3, 1, 2, 2, 2, 1, 2, 2, 3, 1, 2, 1, 1, 1, 2, 1, 1,1, 2, 2, \\1, 1, 1, 84.9, 49.8, 7.8, 61.5, 78.3, 91.6\}
 %\ee
%\] 
%The $p-values$ for each goal are produced from the records through 
Based on these records, significance tests are then conducted for each goal respectively.
For goal $g_1$, feature \emph{ViewListIn} ($f_2$) gets p-value $0.00048$, and feature \emph{ViewProductIn} ($f_9$) gets p-value $0.0003$, both less than $\alpha$ value 0.0025.
Since $f_2$ and $f_9$ are statistically significant for $g_1$, these two features will be included in $F_{goal}^{g_1}$ and accordingly $F_{goal}$. 

The same process is performed for the other five goals, which reveals significant features \emph{AddCartEncryption} ($f_{11}$) and \emph{ViewCartEncryption} (f$_{14}$) for $g_2$,  \emph{VerificationType} ($f_{22}$) and \emph{VerificationDesign}  ($f_{23}$) for $g_3$, $f_2$ and $f_9$ for $g_4$,  $f_{11}$, f$_{14}$ and $f_{22}$ for $g_5$, and \emph{ViewListCaching} ($f_{3}$) and \emph{ViewProductCaching} ($f_8$) for $g_6$.
Therefore $F_{goal}$ is \{$f_2$, $f_3$, $f_8$, $f_9$, $f_{11}$, $f_{14}$, $f_{22}$, $f_{23}$\} under $\alpha$ 0.0025.
A larger $\alpha$ makes more features included in $F_{goal}$, and a smaller $\alpha$ makes less features included in $F_{goal}$.

\subsection{Relationship Modeling}
This step is to model the relationship between goals and the goal-related features as functions (namely, goal-feature functions).
The resulted function can be used to predict goal satisfaction degrees based on the current feature binding states.
In order to learn such functions, the learner need to collect numbers of records stating the values of features and goals, and then estimate a relationship function from the collected records. The estimation does not tie to specific techniques, and can take use of techniques like the linear regression, polynomial regression and m5 model tree \cite{elkhodary2010fusion}, and also other machine learning techniques. 


%The predictor model can be chosen from the linear model, the polynomial model and all other regression models  \cite{elkhodary2010fusion}. 
%Different models can be tried to select an ideal one with which the collected data fit the model best. 

 %in the sense that a model is ideal if it can be used to make good prediction of goal satisfaction degrees with the estimated model parameters.  For example, if we choose the linear regression model which takes the form
%\[
%\bb
 %v_{g}=\beta_1 v_{f_1}+\beta_2 v_{f_2}+ ... +\epsilon
%\ee
%\],
%this model is ideal if the error term $\epsilon$ is acceptable when the values of unknown parameter $\beta$ have been estimated from the collected records.

Observed records need to be collected to support function estimition.
Different from $r$ used in Section \ref{subsec:division}, here a record $r_{goal}$ consists of the value of goals and the value of features in $F_{goal}$.
 \[
 \bb
r_{goal}=\{v_{f_{s1}}, v_{f_{s2}}, ... ,v_{g_1},v_{g_2}...\}
 \ee
\] 
where $f_{si}$ is the ith feature in $F_{goal}$.
The number of independent variables have been greatly reduced since $F_{rule}$ does not need to be involved. 
As stated in \cite{michalski2013machine}, for a regression problem with large number of independent variables, large amounts of records are needed to infer an accurate model. 
The more variables are involved in the regression problem, the more observed records are needed.
Less records are needed with less independent variables, which makes it easier to collect sufficient observed records timely.   

%As stated in Section \ref{}, features have various levels of impacts on goals. 
The learner then estimates the goal-feature function, in the sense that it estimates the unknown parameters of a predictor function.
Assume the predictor function takes the form
\[
\bb
v_{g}'= \beta_1 v_{f_{s1}}+\beta_2 v_{f_{s2}} + \beta_3 v_{f_{s1}}^2 + \beta_4 v_{f_{s2}} ^2+...
\ee
 \]
, where $v_{g}'$ denotes the predicted value of $v_{g}$, the leaner will estimate the values of $\beta$ with which the collected records can well fit the resulted goal-feature function. 
% a good prediction of $v_g$ can be made based on the collected records. 
Note that the goal-feature function might be a segmented function when it is considerable influenced by a context feature.

 
\textbf{Example.}
Back to the running example, the system collected observed records as Table \ref{tab:records2} shows.
 
\begin{table}[htdp]
\caption{Collected Records for Relationship Modeling}
\begin{center}
\begin{tabular}{|c|c|c|c|c|c|c|c|c|c|}
\hline
$f_2$&$f_3$&$f_8$&$f_9$&$f_11$&...& $g_1$&$g_2$&$g_3$&...\\
\hline
3&3&1&2&2&...&84.9&49.8&7.8&...\\
\hline
3&3&1&2&2&...&84.9&49.8&7.8&...\\
\hline
3&3&1&2&2&...&84.9&49.8&7.8&...\\
\hline
3&3&1&2&2&...&84.9&49.8&7.8&...\\
\hline
\end{tabular}
\end{center}
\label{tab:records2}
\end{table}%

The goal-feature functions shall be estimated from these records. 
Suppose a linear predictor function is chosen, the goal-feature function for $g_1$ can be learnt through linear regression with it significant features $f_2$ and $f_9$ as independent variables.
%as the aggregation of $Impact_{f_2, g_1} (v_{f_2})$ and $Impact_{f_9, g_1} (v_{f_9})$.
In this case, the goal-feature function is more likely a segmented function since the impacts from features to goals are considerable influenced by the workload level.
%The impacts are more likely linear when workload level is low, and polynomial when workload level is high. 


\[
\bb
v_{g_1}'=\\~~\left\{\begin{array}{lll}
9.41+17.91\times v_{f_2}+11.09 \times v_{f_9}~workloadLevel<3\\ 
0.91+4.37\times v_{f_2}^2+3.65 \times v_{f_9}^2 ~~~~workloadLevel\geq 3
\end{array}\right.
\ee
\]

Similarly the learner estimates goal-feature functions for other goals.

\[
\bb
\left\{\begin{array}{lll}
v_{g_2}'=9.75+1.06\times v_{f_{11}}+9.15 \times v_{f_{14}}
\\ v_{g_3}'=48.81+16.49\times v_{f_{22}}-3.67 \times v_{f_{23}}
\\ v_{g_4}'=72.98-12.63\times v_{f_{2}}-11.65 \times v_{f_{9}}
\\ v_{g_5}'=143.69-23.20\times v_{f_{22}}-12.27\times v_{f_{11}}-11.2\times v_{f_{14}}
\\v_{g_6}'=-85.93+46.51\times v_{f_{3}}+44.51 \times v_{f_{8}}
\end{array}\right.
\ee
\]
