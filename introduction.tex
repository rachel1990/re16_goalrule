\section{Introduction}
\label{sec: introduciton}
Goal-oriented adaptation (\cite{elkhodary2010fusion}, \cite{DBLP:conf/icse/ChenPYNZ14},  \cite{DBLP:conf/icse/GhezziPST13}, \cite{DBLP:conf/re/SalehiePOAN12}) provides a powerful mechanism to develop self-adaptive systems, enabling systems to keep satisfying goals in a dynamically changing environment.
It normally reduces the dynamic adaptation as an {\em optimization process} and leaves the system the task to reason on the
actions required to achieve high-level goals,
and can be well combined with online learning process to improve the accuracy of adaptation decisions  (\cite{DBLP:journals/tse/EsfahaniEM13}, \cite{DBLP:conf/re/QianPCMWZ14}). 

Thanks to the automatic optimization and the learning process,
goal-oriented adaptation is capable of dealing with uncertainty and keeping making optimal adaptation decisions even when unforeseen conditions occur. 
However, searching for optimal decisions is often computationally expensive and encounters less-efficiency problems.
For a big system with very large searching space, the optimization process 
would become impossible since consumed time grows exponentially with the increases of system features.
For example, the online e-commerce system in Figure \ref{fig:cfm} (adapted from \cite{DBLP:conf/icse/ChenPYNZ14}) has 24 configurable features, which would yield one billion possible configurations in the search space. 
If we would like to take all 24 features together with their enum values into account in the optimization process, it would take nearly one hour to search for optimal solutions for each adaptation.

Such less-efficiency problem would prevent a self-adaptive system from reacting timely to arising situations at run-time, which is not tolerable since efficiency of planning is of utmost importance in most self-adaptive systems (\cite{Chen09}, \cite{DBLP:conf/dagstuhl/Lemos}).
For example, if the aforementioned e-commerce website cannot react timely when it suddenly encounters a traffic peak,
its goal of ``\emph{response in time}'' will fail and its customers might leave. 

Moreover, too many features would make the learning process difficult.
A self-adaptive system needs to learn from runtime behaviors to cope with the changing environment,
and this process requires large numbers of observed data to infer an accurate model to support the optimization process. 
The more features are involved in the learning process, the more observed data is needed.
For a sizable system with too many features, it is often difficult to collect sufficient observed data timely.

To overcome these limitations, we propose to shrink the searching space
in the optimization process and the amount of data needed in the learning process by reducing the number of features. Our idea is to divide the whole features into two sets, so that the features in one set can be quickly handled by the goal-oriented adaptation to guarantee certain degree of goal satisfaction, while the features in the other will be handled by the rule-based adaptation (\cite{Lane10}, \cite{garlan2004rainbow}, \cite{acher2009modeling}) (which has the advantage of efficient planning process) based on the results got from the goal-oriented adaptation. 

To this end, we should (1) find an effective way to divide the features so that the subset of the features we have chosen for the goal-oriented adaptation will lead to good goal satisfaction, and (2) guarantee that propagating the adaptation results to the rest features by the rule-based adaptation will do better, not bringing any bad effect on the results we have got. 

Recall that an adaptation rule typically takes the form of 
``$\m{condition} \Rightarrow \m{action}$''
where \emph{condition} specifies the trigger of the rule, which is
often fired as a result of a set of monitoring operations, and \emph{action}
specifies an operation sequence to perform in response to the trigger.
For instance, in the e-commerce system, we may have the following rule
\[
\small
\bb
\m{VerificationType} = \m{Strict}~\wedge~\m{Workload} = \m{High}\\
\quad \Rightarrow \m{PayLog} := \m{None};~\m{PayEncryption} := \m{None}
\ee
\]
\noindent saying that when ``verification is strict'' and ``workload is high'', we should turn off logging and encryption.
Generally, the action of an adaptation rule might be conflict
with goals when the context/environments are changed,
so it is not easy to combine
the goal-oriented adaptation and the rule-based adaptation.
%While the binding states of some features can be decided by rules, only a subset of features are involved in the optimization process and accordingly searching will become much more efficient.
%What's more, with a significantly pruned configuration space, the learning process needs less number of observed data to obtain a valid analytical model. 
Now which features shall be involved in the goal-oriented optimization?
How can we guarantee that each individual rule {\em preserves} goals with 
synergy between 
the goal-oriented and the rule-based adaptations?

In this paper, we propose a novel adaptation framework that can combine the advantages of both 
learning-based goal-oriented adaptation and the rule-based adaptation. 
Goal-oriented planning works to optimize goals on an important set of
features, while the rule-based planning works to preserve goal-related states and propagate changes to other features. 

In the proposed framework \footnote{\scriptsize{The framework is available at  {http://www.prg.nii.ac.jp/members/stefanzan/viewrule.html}}.}, features are automatically divided into two sets according to their impacts on goals, where a feature will be included in the set for goal-oriented planning only if it has significant impact on one or more goals.
%A feature will be recognized as a significant feature and involved in the goal-oriented planning process only if it has significant impact on one or more goals. 
%Such features are named as significant feature in this article.
The goal-oriented planning works on those significant features to assign them values for goal optimization, and constructs a well-behaved goal-related rule set at the same time. 
The rule-based planning propagates changes to other features with the support of the constructed rule set.
It preserves the feature assignment results of goal-oriented planning (namely, \emph{view}) since our dynamic rule construction algorithm can omit the rules that do harm to \emph{view} and goals.
%Here a concept view is introduced to name the feature assignment results, which will be preserved by rule-based planning with a goal-related rule set. 
%At the same time,  is constructed to support further rule-based planning. 
Both \emph{view} and the rule set will be kept until another goal-oriented planning is triggered by the change of goal settings. 

%This optimal configuration is named as \emph{view}, which will be preserved by rule-based planning to guarantee goal satisfaction.   
%\emph{View} and the goal-related rule set will be kept until 
%This will be triggered when goals or critical attributes have changed.



%As far as we are aware, this is the first attempt of integrating goal-oriented and rule-based adaptations
%A feature will be involved in goal-oriented planning if it has significant impact on one or more goals, and be involved in rule-based planning otherwise.
%The results of goal-oriented planning  
%The results of goal-oriented planning is a goal-related 
%The framework has been implemented with two newly developed algorithms.
%One is for automatic division of the feature set into two sub sets, and the other is for dynamic construction of well-behaved goal-related rule set.

We have applied the framework to design a self-adaptive e-commerce website and a smart room system. 
Our experimental results show that the proposed framework outperforms both a traditional goal-oriented approach and a traditional rule-based approach in terms of adaptation efficiency and effectiveness.

The rest of this paper is structured as follows. Section \ref{sec:preliminary} introduces some preliminaries required before Section \ref{sec:framework} presents our framework. Section \ref{sec:planning} details the integrated planning process. Section \ref{sec:evaluation} evaluates the proposed framework. Section \ref{sec: relatedWork} introduces and compares some related work before Section \ref{sec:conclusion} draws our conclusions. 


\begin{figure}[t]
\begin{center}
\includegraphics[width=0.5\textwidth]{pic/context.pdf}
\caption{An Example Context Feature Model}
\label{fig:cfm}
\end{center}
\end{figure}