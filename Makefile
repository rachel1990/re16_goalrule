all:
	pdflatex  main.tex

bib:
	latex main.tex
	bibtex main
	bibtex main
	pdflatex main.tex

clean:
	rm *.log *.aux *.pdf *.dvi *.bbl *.blg
